from nectar import ec2_conn
import time

def new_instance(image, key_name, type, placement, security_groups, vol_size):
    reservation = ec2_conn.run_instances(image,
                                         key_name=key_name,
                                         instance_type=type,
                                         security_groups=security_groups,
                                         placement=placement)

    instance = reservation.instances[0]
    print('New instance {} has been created.'.format(instance.id))

    vol_request = ec2_conn.create_volume(vol_size, 'melbourne-qh2')
    print('New volume {} created of size {}GB'.format(vol_request.id, vol_size))

    while instance.state != 'running':
        print('Instance: {} --- {}'.format(instance.id, instance.state))
        time.sleep(2)
        instance.update()

    instance_ip = instance.private_ip_address
    print('Instance with IP {} now running.'.format(instance_ip))

    ec2_conn.attach_volume(vol_request.id, instance.id, '/dev/vdc')
    print('Volume {} attached to instance {} at /dev/vdc'.format(vol_request.id, instance.id))
    return instance


passwords = {}

new_inst = new_instance('ami-00003837', 'Michael', 'm1.medium', 'melbourne-qh2', ['default', 'SSH'], 2)
new_host = new_inst.private_ip_address

with open('ansible/hosts', 'a') as hosts:
    hosts.write(new_host + '\n')
    print("New host has been added with hostname {}".format(new_host))

