numpy
CouchDB==1.2
emoji==0.5.0
nltk==3.2.5
textblob==0.15.1
reverse-geocoder==1.5.1
tweepy==3.6.0

